function deepClone(obj) {
    if (obj === null || typeof obj !== 'object') {
      return obj;
    }
  
    let clone;
  
    if (Array.isArray(obj)) {
        clone = [];
        let index = 0;
        for (let item of obj) {
          clone[index++] = deepClone(item);
        }
    } else {
      clone = {};
      for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
          clone[key] = deepClone(obj[key]);
        }
      }
    }
  
    return clone;
  }
  